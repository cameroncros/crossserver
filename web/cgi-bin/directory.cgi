#!/usr/bin/perl -w
use strict;
use warnings;
use CGI;
use CGI::Carp qw ( fatalsToBrowser );
use File::Basename;

print "Content-type: text/html\n\n";


my $query = new CGI;
#my $dir = $query->url(-absolute=>1);
my $dir = $query->param('dir');

$dir =~ s|%20|\ |g;
$dir =~ s|%28|\(|g;
$dir =~ s|%29|\)|g;

my $head = $dir ;
$head =~ s|^/||g;
$head =~ s|/$||g;
$head =~ s|%20| |g;
$head =~ s|%28|(|g;
$head =~ s|%29|)|g;

$head =~ s/(\w+)/\u$1/g ;


print <<"EOF";
<ol>
EOF
my @files;
my $index;
my $filesize;

opendir(DIR, "/var/www/html/$dir");
@files = readdir(DIR);
closedir(DIR);
@files = sort @files;

$index = 0;
foreach (@files) {
if ($_ ne '..' & $_ ne '.' & $_ ne 'index.html') {
	print <<"EOF"; 
<li>
	<div>
	<a href="$dir$_">$_</a>  
EOF
$filesize = -s "/var/www/html/$dir/$_";
if ($filesize < 1024) {
printf(" - %.2f B", $filesize );
}
if ($filesize > 1024 && $filesize < 1048576) {
printf(" - %.2f KB", $filesize/1024 );
}
if ($filesize > 1048576 && $filesize < 1073741824 ) {
printf(" - %.2f MB", $filesize/1048576 );
}
if ($filesize > 1073741824) {
printf(" - %.2f GB", $filesize/1073741824 );
}
print "<div style='float: right'>";
if (isMedia($_) == 1) {
print "<input type='button' value='Preview' onclick='preview(\"$_\")'/>";
}

print <<"EOF";
	<input type='button' value='Delete' onClick='window.location="/cgi-bin/deletefile.cgi?file=$dir$_"'/>
EOF


print <<"EOF";
</div>
</div>
</li>
EOF

	}
}

print <<"EOF";
</ol>
EOF


sub isMedia {
my $file = shift;
my $ext = ($file =~ m/([^.]+)$/)[0];
 if ($ext =~ m/mp3|mp4|jpg|avi|png/ ) {
  return 1;
 }
}
