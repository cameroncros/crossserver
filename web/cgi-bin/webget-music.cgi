#!/usr/bin/perl -w
 
use strict;  
use CGI;  
use CGI::Carp qw ( fatalsToBrowser );  
use File::Basename;  
 
my $upload_dir = "/var/www/download";  
 
my $query = new CGI;  
my $filename = $query->param("file");  
 
if ( !$filename )  
{  
 print $query->header ( );  
 print "There was a problem uploading your url? You didn't put one in the entry?";  
 exit;  
}  

my $out =  `wget -P /var/www/download/ $filename`;

print $out;

print $query->redirect( -URL => "../download");

print $query->header ( );  

print "window.location=\"cameron.servebeer.com\";\n\n";
