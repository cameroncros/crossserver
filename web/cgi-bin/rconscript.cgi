#!/usr/bin/perl -w  
 


use strict;  
use CGI;  
use CGI::Carp qw ( fatalsToBrowser );  
use File::Basename;  
use Switch;
 
my $query = new CGI;  
my $pass = $query->param("pass");  
my $server = $query->param("server");
my $command = $query->param("command");

if ($command eq "status") {
my $results = `./rcon.cgi $server $pass $command`;
my @lines = split('\n' , $results);
print "Content-type: text/html\n\n";

my $map = $lines[2];
$map =~ s/map: mp_//;
switch ($map) {
	case "backlot" {print "<h2>Backlot</h2>"}
	case "bloc" {print "<h2>Bloc</h2>"}
	case "bog" {print "<h2>Bog</h2>"}
	case "broadcast" {print "<h2>Broadcast</h2>"}
	case "caretan" {print "<h2>Chinatown</h2>"}
	case "cargoship" {print "<h2>Wet Work</h2>"}
	case "citystreets" {print "<h2>District</h2>"}
	case "convoy" {print "<h2>Ambush</h2>"}
	case "countdown" {print "<h2>Countdown</h2>"}
	case "crash" {print "<h2>Crash</h2>"}
	case "crash_snow" {print "<h2>Winter Crash</h2>"}
	case "creek" {print "<h2>Creek</h2>"}
	case "crossfire" {print "<h2>Crossfire</h2>"}
	case "farm" {print "<h2>Downpour</h2>"}
	case "killhouse" {print "<h2>Killhouse</h2>"}
	case "overgrown" {print "<h2>Overgrown</h2>"}
	case "pipeline" {print "<h2>Pipeline</h2>"}
	case "shipment" {print "<h2>Shipment</h2>"}
	case "showdown" {print "<h2>Showdown</h2>"}
	case "strike" {print "<h2>Strike</h2>"}
	case "vacant" {print "<h2>Vacant</h2>"}
	else {print "<h2>$map</h2>"}	
}

print "<img src=./${map}logo.jpg img align=right></img>";

print "<table>";

shift(@lines);
shift(@lines);
shift(@lines);
shift(@lines);
shift(@lines);
        print << "EOF";
                <tr>
                        <td>Name</td> <!--name-->
                        <td></td> <!--kick-->
			<td></td> <!--kick-->
			<td></td> <!--kick-->
			<td>Score</td> <!--score-->
                        <td>Ping</td> <!--ping-->
                        <td>Country</td> <!--country-->
                </tr>
EOF
foreach (@lines) {
my $name = substr($_, 47, 21);
my $ip = substr($_, 74, 16);
my $id = substr($_, 0, 3);
my @array = split(/\s+/, $id);
my $id = $array[1];
my $score = substr($_, 4, 5);
my $ping = substr($_, 11,4);
       # my @words = split(/\s+/, $_, 9);
my @iparray = split(":", $ip);
my $ip = $iparray[0];
my @iparray = split(/\s+/, $ip);
my $ip = $iparray[-1];
my $country = `geoiplookup $ip`;
#	my @countryarray = split(":", $country, 2);
#	my $country = $countryarray[2];
#	if ($country == "") {
#		$country="INTERNAL"; 
#	}
my $name = "<div id=black>$name</div>"	;
$name =~ s|\^1|</div><div style="color:rgb(255,0,0);display:inline">|g;
$name =~ s|\^2|</div><div style="color:rgb(0,255,0);display:inline">|g;
$name =~ s|\^3|</div><div style="color:rgb(255,255,0);display:inline">|g;
$name =~ s|\^4|</div><div style="color:rgb(0,0,255);display:inline">|g;
$name =~ s|\^5|</div><div style="color:gb(0,255,255);display:inline">|g;
$name =~ s|\^6|</div><div style="color:rgb(255,0,255);display:inline">|g;
$name =~ s|\^7|</div><div style="color:rgb(128,128,0);display:inline">|g;
$name =~ s|\^8|</div><div style="color:rgb(255,255,255);display:inline">|g;
$name =~ s|\^9|</div><div style="color:rgb(192,192,192);display:inline">|g;
$name =~ s|\^0|</div><div style="color:rgb(255,255,255);display:inline">|g;

	print << "EOF";
		<tr>
			<td>$name</td> <!--name-->
			<td><input type=button onclick="kick($id)" value="Kick"></input></td> <!--kick-->
			<td><input type=button onclick="ban($id)" value="Ban"></input></td> <!--kick-->
			<td><input type=button onclick="tell($id)" value="Say"></input></td> <!--kick-->
			<td>$score</td> <!--score-->
			<td>$ping</td> <!--ping-->
			<td>$ip - $country</td> <!--country-->
		</tr>
EOF
}
print "</table>";


 } elsif ($command =~ m/messages/) {
print "Content-type: text/html\n\n";
	my @array = split(/\s+/ , $command);
	my $no = $array[1];
	$no =~ s/"//g;
	my $messages = `more /media/Games/CallOfDuty4/main/games_mp* | grep say | tail -n $no `;
	my @line = split('\n', $messages);
	print "<table>";
	foreach (@line) {
		my @parts = split(";", $_);
		my $said = substr($parts[-1],1);
		print "<tr><td>$parts[-2] - $said</td></tr>";	
	}
	print "</table>";
 } else {
my $results = `./rcon.cgi $server $pass $command`;
print "Content-type: text/html\n\n";
print "$command\n$results";
}

