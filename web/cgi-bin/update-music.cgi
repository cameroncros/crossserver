#!/usr/bin/perl -wT  

use strict;
use MP3::Tag;
use MP3::Info;
use Digest::SHA;
use File::Basename;  
use DBI;

my $host = "localhost";
my $database = "music";
my $user = "cameron";
my $pw = "cameron";

my $connect = DBI->connect("dbi:mysql:$database", $user,$pw,  { 'PrintError' => 1, 'RaiseError' => 1 });

my $dateadded = time();

my $upload_dir = "/media/Music";  
opendir(DIR, $upload_dir);
my @files = readdir(DIR);
closedir(DIR);

foreach my $filename (@files) {
if ($filename =~ m/.mp3$/) {
print "$filename\n";
my $mp3 = new MP3::Info("$upload_dir/$filename");
if ($mp3 != undef) { 
my $title = $mp3->title;
my $artist = $mp3->artist;
my $album = $mp3->album;
my $comment = $mp3->comment;
my $year = $mp3->year;
my $genre = $mp3->genre;
my $track = 1; #$mp3->track;
$title =~ s/'/\\'/g;
$artist =~ s/'/\\'/g;
$album =~ s/'/\\'/g;

my $sha = Digest::SHA->new();
 $sha->addfile("$upload_dir/$filename");
my $shahash = $sha->hexdigest();
my $query = "DELETE FROM songs WHERE sha1='$shahash';";
my $sql_handle = $connect->prepare($query);
$sql_handle->execute();
$filename =~ s/'/\\'/g;

$query = "INSERT INTO songs VALUES ('$title', '$artist', '$album', '$track', '$year', 1, '$shahash', '$dateadded', '$upload_dir/$filename');";
#my $query = "INSERT INTO songs VALUES ("$title", "$artist", "$album", "$track', '$year', 1, '$sha->hexdigest');";
$sql_handle = $connect->prepare($query);
$sql_handle->execute();
}
}
}
