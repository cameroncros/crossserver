#!/usr/bin/perl

use CGI;
use DBI;
$cgiquery = new CGI;
my $url = $cgiquery->param('addr');

print "Content-type: text/html\n\n";

my $host = "localhost";
my $database = "music";
my $user = "cameron";
my $pw = "cameron";

$connect = DBI->connect("dbi:mysql:$database", $user,$pw,  { 'PrintError' => 1, 'RaiseError' => 1 });


$url =~ s/%20/ /g;
$url =~ s/%28/(/g;
$url =~ s/%29/)/g;

while ($url =~ m|//|) {
	$url =~ s|//|/|g;
}

my @parts = split "/","$url";
if ($parts[2] =~ m/library/) {
	dispLibrary(@parts);
}
elsif ($parts[2] =~ m/index/) {
	dispIndex(@parts);
}
elsif ($parts[2] =~ m/search/) {
	dispSearch(@parts);
}
elsif ($parts[2] =~ m/uploaded/) {
	dispRecent();
}



sub dispLibrary {
shift;
shift;
shift;
my $artist = shift;
$artist =~ s/%20/ /;
my $album = shift;
my $song = shift;
my $dataquery;

if ($artist eq undef) {
$dataquery = "SELECT * FROM songs ORDER BY artist,album,trackno;";
} elsif ($album eq undef) {
$dataquery = "SELECT * FROM songs WHERE artist = '$artist' ORDER BY artist,album,trackno;";
} elsif ($song eq undef) {
$dataquery = "SELECT * FROM songs WHERE artist = '$artist' AND album = '$album' ORDER BY artist,album,trackno;";
} else {
$dataquery = "SELECT * FROM songs WHERE artist = '$artist' AND album = '$album' AND title='$song' ORDER BY artist,album,trackno;";

}
print $artist;
print $album;
print $song;
print $dataquery;
my $execute = $connect->prepare($dataquery);
$execute->execute();
if ($execute->rows() != 0) {
while (@results = $execute->fetchrow()) {
        my $songstr = printSong(@results);
	print "<p>$songstr</p>";
}
}
}
sub dispIndex {
	shift;
	shift;
	shift;
	my $letter = shift;
	if ($letter =~ m/[A-Z]/) {
		print "<h3>Artists beginning with $letter</h3>";
$dataquery = "SELECT * FROM songs WHERE UPPER(artist) LIKE '$letter%' ORDER BY artist,album,trackno";
my $execute = $connect->prepare($dataquery);
$execute->execute();
my $count = 0;
 if ($execute->rows() != 0) {
  while ((@results = $execute->fetchrow()) != 0) {
        my $songstr = printSong(@results);
        print "<li>$songstr</li>";
  }
 } else {
	print "<h3>No Artists Begin with $letter</h3>";
 }

	} else {
	print "<ul>";
	for (A .. Z) {
		print "<li onclick=\"setLocation('/music/index/$_/')\">$_</li>";
	}
	print "</ul>";
	}
}
sub dispSearch {
	shift;
	shift;
	shift;
	my $searchTerm = shift;
	print "<h3>Searching for:$searchTerm</h3>";
$dataquery = "SELECT * FROM songs WHERE artist LIKE '%$searchTerm%' OR album LIKE '%$searchTerm%' OR title LIKE '%$searchTerm%' OR year LIKE '%$searchTerm%' ORDER BY artist,album,trackno";
my $execute = $connect->prepare($dataquery);
$execute->execute();
 if ($execute->rows() != 0) {
  while ((@results = $execute->fetchrow()) != 0) {
        my $songstr = printSong(@results);
        print "<li>$songstr</li>";
  }
 } else {
	print "<h4>No results found.<h4>"
 }
}

sub dispRecent {
$dataquery = "SELECT * FROM songs ORDER BY date,artist,album,trackno";
my $execute = $connect->prepare($dataquery);
$execute->execute();
 if ($execute->rows() != 0) {
  print "<ol>";
  while ((@results = $execute->fetchrow()) != 0 && $count < 10) {
        my $songstr = printSong(@results);
	print "<li>$songstr</li>";
	$count =$count+1;
  }
  print "</ol>";
 }
}

sub printSong {
	my $title = shift;
	my $artist = shift;
	my $album = shift;
	my $trackno = shift;
	my $year = shift;
	my $diskno = shift;
	my $sha1 = shift;
	my $date = shift;
	my $file = shift;
	return "<a onclick=\"setLocation('/music/library/$artist')\">$artist</a> - <a onclick=\"addToPlaylist('$file\')\">$title</a> - <a  onclick=\"setLocation('/music/library/$artist/$album')\">$album</a> - $trackno - $year";
}	

