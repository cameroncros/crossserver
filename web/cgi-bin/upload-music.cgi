#!/usr/bin/perl -wT  

use strict;
use CGI;  
use CGI::Carp qw ( fatalsToBrowser );  
use MP3::Tag;
use MP3::Info;
use Digest::SHA;
use File::Basename;  
use DBI;

my $host = "localhost";
my $database = "music";
my $user = "cameron";
my $pw = "cameron";

my $connect = DBI->connect("dbi:mysql:$database", $user,$pw,  { 'PrintError' => 1, 'RaiseError' => 1 });

my $dateadded = time();

$CGI::POST_MAX = 1024 * 5000000;  
my $safe_filename_characters = "a-zA-Z0-9_.-";  
my $upload_dir = "/media/Music";  
 
my $query = new CGI;  

my @files = $query->param("file");  
my @handles = $query->upload("file");

if ( !@files )  
{  
 print "There was a problem uploading your file (maybe it is too large?).";  
 exit;  
}  


my $filename;
my $upload_filehandle;

$filename = shift(@files);
$upload_filehandle = shift(@handles);
while ($filename ne undef && $upload_filehandle ne undef) {
#while  (($filename = shift(@files)) != undef && ($upload_filehandle = shift(@handles)) != undef) {
print "$filename $upload_filehandle";
my ( $name, $path, $extension ) = fileparse ( $filename, '\..*' );  
$filename = $name . $extension;  
$filename =~ tr/ /_/;  
$filename =~ s/[^$safe_filename_characters]//g;  
 
if ( $filename =~ /^([$safe_filename_characters]+)$/ )  
{  
 $filename = $1;  
}  else  
{  
 die "Filename contains invalid characters";  
}  

#if (file does# exist) { 
# appendnumebes until it does
# deal with this issue someway (temp file?)
#}
open ( UPLOADFILE, ">$upload_dir/$filename" ) or die "$!";  
binmode UPLOADFILE;  
 
while ( <$upload_filehandle> )  
{  
 print UPLOADFILE;  
}  
 
close UPLOADFILE;  
#my $mp3data = MP3::Tag->new("$upload_dir/$filename");
my $mp3 = new MP3::Info("$upload_dir/$filename");
my $title = $mp3->title;
my $artist = $mp3->artist;
my $album = $mp3->album;
my $comment = $mp3->comment;
my $year = $mp3->year;
my $genre = $mp3->genre;
my $track = $mp3->track;
 #($title, $track, $artist, $album, $comment, $year, $genre) = $mp3data::MP3->autoinfo();
$title =~ s/'/\\'/g;
$artist =~ s/'/\\'/g;
$album =~ s/'/\\'/g;

my $sha = Digest::SHA->new();
 $sha->addfile("$upload_dir/$filename");
my $shahash = $sha->hexdigest();
my $query = "DELETE FROM songs WHERE sha1='$shahash';";
my $sql_handle = $connect->prepare($query);
$sql_handle->execute();
$query = "INSERT INTO songs VALUES ('$title', '$artist', '$album', '$track', '$year', 1, '$shahash', '$dateadded', '$upload_dir/$filename');";
#my $query = "INSERT INTO songs VALUES ("$title", "$artist", "$album", "$track', '$year', 1, '$sha->hexdigest');";
$sql_handle = $connect->prepare($query);
$sql_handle->execute();
$filename = shift(@files);
$upload_filehandle = shift(@handles);
}



print $query->redirect( -URL => "/music/uploaded/");
print $query->header();
print "window.location=\"cameron.servebeer.com\";\n\n";


