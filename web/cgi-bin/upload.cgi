#!/usr/bin/perl -wT  
 


use strict;  
use CGI;  
use CGI::Carp qw ( fatalsToBrowser );  
use File::Basename;  
 
$CGI::POST_MAX = 1024 * 5000000;  
my $safe_filename_characters = "a-zA-Z0-9_.-";  
my $upload_dir = "/var/www/html/download";  
 
my $query = new CGI;  

my @files = $query->param("file");  
my @handles = $query->upload("file");

if ( !@files )  
{  
 print "There was a problem uploading your file (maybe it is too large?).";  
 exit;  
}  


my $filename;
my $upload_filehandle;
while  ($filename = shift(@files)) {
$upload_filehandle = shift(@handles);  
my ( $name, $path, $extension ) = fileparse ( $filename, '\..*' );  
$filename = $name . $extension;  
$filename =~ tr/ /_/;  
$filename =~ s/[^$safe_filename_characters]//g;  
 
if ( $filename =~ /^([$safe_filename_characters]+)$/ )  
{  
 $filename = $1;  
}  else  
{  
 die "Filename contains invalid characters";  
}  
 
 
open ( UPLOADFILE, ">$upload_dir/$filename" ) or die "$!";  
binmode UPLOADFILE;  
 
while ( <$upload_filehandle> )  
{  
 print UPLOADFILE;  
}  
 
close UPLOADFILE;  
}

print $query->redirect( -URL => "../download");
print $query->header();
print "window.location=\"cameron.servebeer.com\";\n\n";

