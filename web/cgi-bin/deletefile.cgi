#!/usr/bin/perl -w 

use strict;  
use CGI;  
use CGI::Carp qw ( fatalsToBrowser ); 
use File::Basename;  
 
 
my $query = new CGI;  

my $safe_filename_characters = "a-zA-Z0-9_.-"; 
my $filename = $query->param("file");  

if ( !$filename )  
{  
 print $query->header ( );  
 print "There was a problem with $filename";  
 exit;  
}
#$filename =~ s/[^$safe_filename_characters]//g;  
 unlink "/var/www$filename";


print $query->redirect( -URL => "/download/");

print $query->header ();  

