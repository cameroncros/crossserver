function showOverlay() {
var overlay = document.getElementById('overlay');
overlay.style.display = 'block';
resizeOverlay();
}

function resizeOverlay() {
var overlay = document.getElementById('overlay');
if (overlay.style.display == 'block') {
var innerOverlay = document.getElementById('innerOverlay');
var contentOverlay = document.getElementById('contentOverlay');
var backgroundOverlay = document.getElementById('backgroundOverlay')
var contentWidth = contentOverlay.clientWidth;
var contentHeight = contentOverlay.clientHeight;
if (contentWidth < 600) {
contentWidth=600;
}
if (contentHeight < 200) {
contentHeight= 200;
}

if (contentWidth > window.innerWidth -40) {
contentWidth= window.innerWidth-40;
}
if (contentHeight > window.innerHeight -40) {
contentHeight= window.innerHeight-40;
}

var vmargin = (window.innerHeight-contentHeight)/2;
var hmargin = (window.innerWidth-contentWidth)/2;
var closeOverlay = document.getElementById('closeOverlay');

backgroundOverlay.style.width = screen.width+"px";
backgroundOverlay.style.height = screen.height+"px";
contentOverlay.style.width = contentWidth;
contentOverlay.style.height = contentHeight;
innerOverlay.style.width = contentWidth+"px";
innerOverlay.style.height = contentHeight+"px";
innerOverlay.style.left = hmargin+"px";
innerOverlay.style.top = vmargin+"px";
closeOverlay.style.left = (window.innerWidth-hmargin-closeOverlay.offsetWidth/2)+"px";
closeOverlay.style.top = (vmargin-closeOverlay.offsetHeight/2)+"px";
}
}

function hideOverlay() {
document.getElementById('overlay').style.display = 'none';
}


function resize() {
	document.getElementById('spacertop').style.height = document.getElementById('header').clientHeight+"px";
	document.getElementById('spacerbot').style.height =document.getElementById('footer').clientHeight+"px";
	document.getElementById('footer').style.top = window.innerHeight-document.getElementById('footer').clientHeight+"px";
	document.getElementById('header').style.width = document.getElementById('content').clientWidth+"px"
	document.getElementById('footer').style.width = document.getElementById('content').clientWidth+"px"
	resizeOverlay();	
}


window.onresize = resize;
window.onload = resize;
