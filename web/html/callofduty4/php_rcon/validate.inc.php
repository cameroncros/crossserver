<?php
//session_save_path('/var/lib/php5');

session_name('php_rcon'); session_set_cookie_params(0,'','',false);
session_start(); header('P3P: CP="NOI ADM DEV PSAi COM NAV OUR OTRo STP IND DEM"');

error_reporting(E_ALL & ~E_NOTICE); // leave this as it is

if (! isset($noredirect))
	{
	if (($_SESSION['hasadminrights'] < 1) || ($_SESSION['appdir'] != getcwd()))
		{
		session_destroy();
		header ('Location: login.php');
		exit;
		}
	}
?>
