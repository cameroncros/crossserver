<?php
$server_login_error = '<h2>Error: Not logged in.</h2>';

require ('init.inc.php');

$corrupt_response_separator = "\xff\xff\xff\xffprint\n";
$corrupt_replacement = '<span style="background-color: #663C15">&nbsp;</span>';

$a = $_POST['a'];
$b = $_POST['b'];
$c = $_POST['c'];
$d = $_POST['d'];


if ($a == 'suggest')
	{
	echo '=';
	
	if (! is_file('commands/'.$game.'.txt')) {exit;}
	$fp = @ fopen('commands/'.$game.'.txt','r');
	if (! $fp) {exit;}

	$c = strtolower($b);
	$d = strlen($c);
	if (($suggest_partial) && ($d<2))
	    exit;
	while (!feof($fp))
		{
    	$buf = fgets($fp, 4096);
    	$ok = false;
		if ($suggest_partial)
		    {
		    $tmp = explode(' ',$buf, 2);
		    $tmp = strtolower($tmp[0]);
			if (strpos($tmp, $c) !== false)
				{
				$ok = true;
				}
			}
			else
			{
			if (strtolower(substr($buf,0,$d)) == $c)
				{
				$ok = true;
				}
			}
		if ($ok)
		    {
			$buf = trim(str_replace('"','',$buf));
			echo $buf."\r\n";
			}
		}

    fclose($fp);
	exit;
	}
	
	
$res = '';
$colors = $c;
$server_buffer_cur = $server_buffer_results;
if (!isset($corrupted_join_char_fix))
	{$corrupted_join_char_fix = false;}

function exitwithmsg($s)
	{
	if ($connect) {fclose($connect);}
	die('<h2>'.$s.'</h2>');
	}

$server_addr = "udp://" . $server_ip;
$connect = @ fsockopen($server_addr, $server_port, $re, $errstr, $server_timeout);
if (! $connect) { exitwithmsg($lang['connection_error']); }
socket_set_timeout ($connect, $server_timeout);

function ColorizeName($s) {
	global $corrupt_response_separator, $corrupt_replacement, $corrupted_join_char_fix;
	$pattern[0]="^0";	$replacement[0]='</font><font color="black">';
	$pattern[1]="^1";	$replacement[1]='</font><font color="red">';
	$pattern[2]="^2";	$replacement[2]='</font><font color="lime">';
	$pattern[3]="^3";	$replacement[3]='</font><font color="yellow">';
	$pattern[4]="^4";	$replacement[4]='</font><font color="blue">';
	$pattern[5]="^5";	$replacement[5]='</font><font color="aqua">';
	$pattern[6]="^6";	$replacement[6]='</font><font color="#FF00FF">';
	$pattern[7]="^7";	$replacement[7]='</font><font color="white">';
	$pattern[8]="^8";	$replacement[8]='</font><font color="white">';
	$pattern[9]="^9";	$replacement[9]='</font><font color="gray">';
 	$pattern[10]=$corrupt_response_separator;	$replacement[10]=(($corrupted_join_char_fix)?$corrupt_replacement:'');

	$s = str_replace($pattern, $replacement, htmlspecialchars($s));
	$i = strpos($s, '</font>');
	if ($i !== false)
		{return substr($s, 0, $i) . substr($s, $i+7, strlen($s)) . '</font>';}
	else
		{return $s;}
	}

function RemoveJoiningChars($s) {
	global $corrupt_response_separator, $corrupt_replacement, $corrupted_join_char_fix;
	$s = htmlspecialchars($s);
	$s = str_replace($corrupt_response_separator, (($corrupted_join_char_fix)?$corrupt_replacement:''), $s);
	return $s;
	}

function RequestToGame($cmd)
	{
	global $server_rconpass, $connect, $server_extra_wait, $server_buffer_cur, $server_extra_footer;
	$send = "\xff\xff\xff\xff\x02" . 'rcon "' . $server_rconpass . '" '.$cmd.(($server_extra_footer)?"\x0a\x00":'');
	fwrite($connect, $send);

	$output = (($server_extra_wait)?(fread ($connect, 1)):'');
		do {
		$status_pre = socket_get_status ($connect);
		if ((($server_extra_wait) && ($output != '')) || (! $server_extra_wait))
			$output .= fread ($connect, $server_buffer_cur);
		$status_post = socket_get_status ($connect);
		} while ($status_pre['unread_bytes'] != $status_post['unread_bytes']);

// 		// Debug server response
// 	    for ($i=0; $i < strlen($output); $i++) {
//         	$t = dechex(ord(substr($output,$i)));
//         	if ($t != 'a')
//             {$output2 .= '\\'.$t;}
//             else {$output2 .= "\n";}
//     		}
// 		$output = $output2;

	return $output;
	}

function LogCommand($s)
	{
	global $log_enable, $admin_name, $server_friendly_name, $lang;
	if ($log_enable === true)
		{
		if (! is_file('log.php'))
		    {
            if (! @copy('log.tpl.php', 'log.php'))
				{
				echo '<h2>'.$lang['log_write_error'].'</h2>';
				return;
				}
			}
        @$fp = fopen('log.php','a');
        if ($fp)
            {
	        fwrite($fp, date('Y-m-d H:i:s').' '.$admin_name.'@'.$server_friendly_name.': '.$s."\n");
	        fclose($fp);
            } else
            {
			echo '<h2>'.$lang['log_write_error'].'</h2>';
			return;
			}
		}
	}

switch ($a)
	{
	case 'plist':
	    //////// playerlist begin
	  	$server_buffer_cur = $server_buffer;
		$output = RequestToGame($b);
		
		if ($output == '') {exitwithmsg ($lang['connection_error']);}
	    echo '~'; //notifies the main script to display the result in playerlist field

		$output = substr($output,strpos($output, $corrupt_response_separator)+strlen($corrupt_response_separator));

		$output = ColorizeName($output);
		$output = explode ("\n", $output);
		$color2 = false;
		$cnt = count($output)-2;

		$output_offset = 0;
		for($i=0; $i<$cnt+2; $i++)
			{if (strpos($output[$i], 'map: ') !== false)
			    {
			    $output_offset = $i;
			    continue;
			    }
			}


		$curmap = substr(str_replace($corrupt_replacement, '', $output[$output_offset]), 5);
		$curmap_orig = $curmap;

		foreach ($list_of_maps as $map)
			{
		    $t = explode(' ',$map,2);
		    if ($t[0] == $curmap)
				{
				$curmap = $t[1];
				break;
				}
			}

		$custom_cmds[] = $lang['kick'].'/clientkick/0';
		$custom_cmdcount = count($custom_cmds);

		echo '<table style="position: relative;">
		<tr class="'.(($color2) ? 'odd' : 'even').'">
		<td align=center'.(($custom_cmdcount>1)?' colspan='.$custom_cmdcount:'').'>
		<div ID="mapimg" onmouseout="MapImgShow(\'\'); return false" onclick="MapImgShow(\'\'); return false" style="position: absolute; top: 3px; left: 3px;"></div>
		<a href="#" onclick="MapImgShow(\''. $game.'/'.$curmap_orig .'\'); return false" title="Hotkey: M" ID="mapimgbtn">'. $curmap .'</a></td>
		<td align=center width="'.$colwidth.'"><a href="#" onclick="CmdMsg(\''.$lang['enter_public_message'].'\',\'say &quot;^6'.$admin_name.' ('.$lang['msg_prefix_all'].'): ^7%m&quot;\',\'\'); return false" title="say">'.$lang['say'].'</a></td>
		'.(($geoip_resolve>0)?'<td align=center>CC</td>':'').'
		<td><pre>'.$output[$output_offset+1]."</pre></td></tr>\n";


		if ($geoip_resolve>0)
			{
			function stringrpos($haystack,$needle,$offset=NULL)
				{
				   if (strpos($haystack,$needle,$offset) === FALSE)
				     return FALSE;

				   return strlen($haystack)
				           - strpos( strrev($haystack) , strrev($needle) , $offset)
				           - strlen($needle);
				}

			function checkValidIp($ip)
				{
				if(!ereg("^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}$", $ip)) $return = FALSE;
				else $return = TRUE;
				$tmp = explode(".", $ip);
				if($return == TRUE){
					foreach($tmp AS $sub){
						$sub = $sub * 1;
				            if($sub<0 || $sub>256) $return = FALSE;
				  }
				}
				return $return;
				}

			function match_network ($nets, $ip) {
			    $return = false;
			    if (!is_array ($nets)) $nets = array ($nets);

			    foreach ($nets as $net) {
			        $rev = (preg_match ("/^\!/", $net)) ? true : false;
			        $net = preg_replace ("/^\!/", "", $net);

			        $ip_arr   = explode('/', $net);
			        $net_long = ip2long($ip_arr[0]);
			        $x        = ip2long($ip_arr[1]);
			        $mask     = long2ip($x) == $ip_arr[1] ? $x : 0xffffffff << (32 - $ip_arr[1]);
			        $ip_long  = ip2long($ip);

			        if ($rev) {
			            if (($ip_long & $mask) == ($net_long & $mask)) return false;
			        } else {
			            if (($ip_long & $mask) == ($net_long & $mask)) $return = true;
			        }
		    	}
		    	return $return;
				}
				
			if ($geoip_resolve == 4)
			    {
                include('geoip.inc.php');
                @$geoip_fp = geoip_open('GeoIP.dat',GEOIP_STANDARD);
                if (! $geoip_fp->filehandle)
                    {
                    echo '<h2>'.$lang['geoipdat_error'].'</h2>';
					}
				}
			}

		for($i=3; $i<$cnt-$output_offset; $i++)
			{
			$line = $output[$output_offset+$i];
			$pat[0] = "/^\s+/";
			$pat[1] = "/\s{2,}/";
			$pat[2] = "/\s+\$/";
			$rep[0] = "";
			$rep[1] = " ";
			$rep[2] = "";
			$t = preg_replace($pat,$rep,$line);

			$t = explode(' ', $t, 2);
		    if (strpos($t[0], '!') !== false) $t[0] = '';
		    $color2 = ! $color2;
		    $is_num = is_numeric($t[0]);
			echo '<tr class="'.(($color2) ? 'odd' : 'even').'">';
			if ($is_num)
				{
				echo '<td align="center" width="'.$colwidth.'">';
				foreach ($custom_cmds as $ccmd)
					{
					$ccmd = explode('/',$ccmd,3);
					if (strpos($ccmd[1], '%m') !== false)
					    {
						$s = ($t[0]+ (int) $ccmd[2]);
						echo '<a href="#" onclick="CmdMsg(\''.$ccmd[1].'\',\''.$ccmd[1].'\',\''.$s.'\'); return false" title="'.$ccmd[1].'">'.$ccmd[0].'</a>';
						} else {
						$s = $ccmd[1].' '.($t[0]+ (int) $ccmd[2]);
						echo '<a href="#" onclick="CustomCmd(\''.$s.'\'); return false" title="'.$s.'">'.$ccmd[0].'</a>';
						}
					echo '</td><td align=center width="'.$colwidth.'">';
					}
					
				if ((! isset($disable_whisper)) || ($disable_whisper === false))
					{echo '<a href="#" onclick="CmdMsg(\''.$lang['enter_message'].'\',\'tell '.$t[0].' &quot;^6'.$admin_name.' ('.$lang['msg_prefix_priv'].'): ^7%m&quot;\',\''.$t[0].'\'); return false" title="tell '.$t[0].'">'.$lang['whisper'].'</a>';}

				} else
			    {
			    echo '<td align="center" colspan="'.($custom_cmdcount+1).'">&nbsp;';
				}

				if ($geoip_resolve>0)
				    {
		            unset($tmp);
		            if (ereg("[[:space:]][0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}", $line, $tmp))
		                {
		                $i_ip = substr($tmp[0],1);
						} else $i_ip = '';

				    echo '</td><td>';
				    $tmp = '';
				    if (strpos($line, ' loopback   ') !== false)
						{$tmp = $geoip_local_network;}
				    if (($i_ip != '') && (strpos($i_ip, $corrupt_replacement) === false) && (checkValidIp($i_ip)))
					    {
					    if ($geoip_local_network != '')
					        {
							if (match_network(array('10.0.0.0/8','192.168.0.0/16','172.16.0.0/12'),$i_ip))
							    {$tmp = $geoip_local_network;}
							}
						if ($tmp == '')
							{
							switch ($geoip_resolve)
								{
								case 1:
									if (function_exists('geoip_country_code_by_name'))
										$tmp = geoip_country_code_by_name($i_ip);
									break;
								case 2:
									$tmp = trim(exec('geoiplookup '.$i_ip));
									$tmp = explode(': ',$tmp,2);
									$tmp = explode(',',$tmp[1],2);
									$tmp = $tmp[0];
									break;
								case 3:
									$tmp = trim(exec('geoip-lookup '.$i_ip));
									break;
								case 4:
									if ($geoip_fp->filehandle)
										$tmp = geoip_country_code_by_addr($geoip_fp, $i_ip);
									break;
								default: ;
								}
							}
						}
					if ($tmp == '')
						$tmp = '--';

					if (($geoip_flags) && (is_file('flags/'.$tmp.'.png')))
					    {echo '<img src="flags/'.$tmp.'.png" alt="'.$tmp.'" title="'.$tmp.'" width=25 height=15 align="absmiddle">';} else
					        {echo $tmp;}

					}

				echo '</td><td><pre>'.$line."</pre></td></tr>\n";
			}
		echo '</table><br>';
		fclose($connect);
		exit;
		//////// playerlist end
		break;

	case 'cmd':
		if ($b != '')
		    {
			$ok = true;
		    if ($rcon_pw_protect)
		        {
		        $tmp = preg_replace("/[^a-z_ ]/",' ', strtolower($b));
				$tmp = explode(' ',$tmp);
				$tmp2 = array_search($rcon_pw_varname, $tmp, true);
				if ($tmp2 !== false)
				    {
                    if ($tmp2 == 0)
						{
						$ok = false;
						} else {
						for ($i=0;$i<$tmp2;$i++)
						    {
							if ($tmp[$i] == 'set') {$ok = false;}
							}
						}
					}
				}
			if ($ok)
			    {
				if ($b != 'g_gametype')
					{LogCommand($b);}
				$res = RequestToGame($b);
				} else
				{
				LogCommand($lang['rcon_pw_protected_error'].': '.$b);
				exitwithmsg($lang['rcon_pw_protected_error']);
				}
			}
		break;

	case 'pass':
		LogCommand('g_password '.(($b!='')?'"*****"':'reset'));
		$res = RequestToGame('g_password "' .$b. '"');
		break;
		
	}

fclose($connect);

if ($res == '')
	{
	exitwithmsg($lang['connection_error']);
	}
	
if ($res != $corrupt_response_separator)
    {
	$res = substr($res,strpos($res, $corrupt_response_separator)+strlen($corrupt_response_separator));
    } else
    {
	$res = 'OK'; // if result value is empty, but valid, return OK as default
	}
	
$res = trim($res);

if ($res != '')
    {
	echo '<pre>'.(($colors == '1')?ColorizeName($res):RemoveJoiningChars($res)).'</pre>';
	}

?>
