<?php
// make sure this file is writeable for the webserver

// add users by creating a new line:
// $list_of_users[] = 'admin_name password';

// they can change their password from the web interface
// when they do, it is stored here in encrypted form
// all passwords are case sensitive


$list_of_users[] = 'admin $1$Rs7Et5Tf$dNIfuwRyxex9FKQLYDy361';
$list_of_users[] = 'cameron $1$Rs7Et5Tf$XX.nDfMcZn0V6KJGcY6eM.';
$list_of_users[] = 'james james';
?>
