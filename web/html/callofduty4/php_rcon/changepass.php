<?php
require ('init.inc.php');

if ((! isset($changepass_enable)) || $changepass_enable == false)
	{
	header ('Location: login.php');
	exit;
	}

echo '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta http-equiv="Cache-Control" content="no-store,max-age=-1">
<title>PHP Rcon - '.$lang['changepass_title'].'</title>
<link rel="stylesheet" type="text/css" href="css.css">
<base target="_self">
</head><body class="padded">
<h1><a href="login.php">Admin</a> / '.$lang['changepass_title'].'</h1>';

echo '<table width="200"><form action="'.$_SERVER['PHP_SELF'].'" method="POST">
<tr>
<td>'.$lang['changepass_old_password'].':</td></tr><tr>
<td align="right"><input class=query type="password" name="old" size="25" AUTOCOMPLETE="off"></td></tr><tr>
<td>'.$lang['changepass_new_password'].':</td></tr><tr>
<td align="right"><input class=query type="password" name="pass" size="25" AUTOCOMPLETE="off"></td></tr><tr>
<td>'.$lang['changepass_confirm_new_password'].':</td></tr><tr>
<td align="right"><input class=query type="password" name="pass2" size="25" AUTOCOMPLETE="off"></td></tr><tr>
<td align="right"><input class="button" type="submit" value=" OK "></form></td>
</tr></table>';

	if (($_POST['old'] != '') || ($_POST['pass'] != '') || ($_POST['pass2'] != ''))
		{
		if (($_POST['old'] != '') && ($_POST['pass'] != '') && ($_POST['pass'] == $_POST['pass2']))
		    {
		    if (strlen($_POST['pass']) >= $changepass_minchars)
				{
				$userl = strtolower($_SESSION['user']);
				$pass = $_POST['old'];
				$passc = crypt($pass, $pw_salt);

				$users = file('users.inc.php');
				$cnt = count($users);
				for($i=0; $i<$cnt; $i++)
				    {
				    $cur = trim($users[$i]);
				    $cur = explode('=',$cur,2);
				    if (trim($cur[0]) == '$list_of_users[]')
				        {
						$cur = explode('\'',$cur[1],3);
						$cur = explode(' ',$cur[1]);

						if ($userl == strtolower($cur[0]))
						    {
							if ((($pass == $cur[1]) && (substr($cur[1],0,1)!='$')) || //plain pw
							   ($passc == $cur[1])) // encrypted pw
								{
								$cur[1] = crypt($_POST['pass'], $pw_salt);;
								$users[$i] = '$list_of_users[] = \''.implode(' ',$cur)."';\r\n";
								if (file_put_contents('users.inc.php', implode('',$users)) !== false)
									{echo $lang['changepass_success'];}
									else
									{echo $lang['changepass_write_error'];}
								} else
								{echo $lang['changepass_error_oldpw'];}
							break;
							}
						}
					}
				}
				else
			{
			$tmp = explode('/',$lang['changepass_error_newpw_short'],2);
			echo $tmp[0] . $changepass_minchars . $tmp[1];
			}

			}
			else
			{echo $lang['changepass_error_newpw'];}
	}

?>
