<?php

require 'validate.inc.php';
require_once 'config.inc.php';
include_once 'language.inc.php';
include_once 'servers.inc.php';

$server_id = (int) $_GET['server'];

list($game, $server_name, $server_friendly_name) = explode(' ',$servers[$server_id],3);

if ($server_friendly_name == '')
	{$server_friendly_name = $server_name;}

if (is_file('games/'.$game.'.inc.php'))
	include('games/'.$game.'.inc.php');

if (is_file('servers/'.$server_name.'.inc.php'))
	include('servers/'.$server_name.'.inc.php');

// set_time_limit(30);         		// maximum script execution time in seconds

$admin_name = $_SESSION['user'];	// used as a prefix of sent messages

$colwidth = 90;						// width of columns to the left of playerlist

?>
