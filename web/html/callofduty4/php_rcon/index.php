<?php
// PHP RCon 2.0
// Created by Ashus in 2008

require ('init.inc.php');

$page_title = '<a href="login.php">Admin</a> / PHP RCon';

echo '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta http-equiv="Cache-Control" content="no-store,max-age=-1">

<title>PHP RCon</title>
<link rel="stylesheet" type="text/css" href="css.css">
<base target="_self">

<script type="text/javascript" src="jquery.pack.js"></script>
<script type="text/javascript" src="jquery.hotkeys.js"></script>
';?>
<script type="text/javascript">
<!--
var defrefreshinterval=<?php echo ((is_numeric($refresh_rate) && ($refresh_rate > 5))?$refresh_rate:'60'); ?>;
var selected_obj;
var waiting_for_data=0;
var queued_suggest=false;
var refreshinterval = defrefreshinterval;
var starttime;
var nowtime;
var reloadseconds=0;
var secondssinceloaded=0;
var stopping=false;


function Startup()
	{
	RefreshNow();
	}

function iPostData(a, b, c, d)
	{
    var http_request = false;
    if ((a==null) || (a.length < 1))
        {return;}
    var request = "a="+escape(a);
    if ((b!=null) && (b.length > 0)) {request += "&b="+escape(b);
		if ((c!=null) && (c.length > 0)) {request += "&c="+escape(c);
			if ((d!=null) && (d.length > 0)) {request += "&d="+escape(d);
			}}}

	if ((a=="cmd") || (a=="pass"))
	    {
		var elem = document.getElementById("res");
		if (elem != null)
			elem.innerHTML = "";
		}

    waiting_for_data++;
    WorkingIndicator();

	// AJAX
    if (window.XMLHttpRequest) {
        http_request = new XMLHttpRequest();
    } else if (window.ActiveXObject) {
        try {
          http_request = new ActiveXObject("Msxml2.XMLHTTP");
        } catch (eror) {
          http_request = new ActiveXObject("Microsoft.XMLHTTP");
        }
    }

    http_request.onreadystatechange = function() { iIncomingData(http_request); };
    http_request.open("POST", "action.php?server=<?php echo $server_id; ?>", true);
    http_request.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    http_request.send(request);
   }

function EncloseToResultTable(content)
	{
	return '<table style="padding-bottom: 13px;"><tr class="even"><td align=center width="<?php echo $colwidth.'">'.$lang['result']; ?>:<br>'
		+'<a href="#" onclick="DeleteResults(); return false" title="Hotkey: X">X</a></td><td>'
		+content;
		+'</td></tr></table>';
	}

function EncloseToSuggestTable(content)
	{
	var rval = '';
	var str = content.split("\n");
	for(i=0; i < str.length - 1; i++)
		{
		var o = str[i].indexOf(' ');
		s = str[i].substring(0,str[i].length-1);
		if (o < 0)
			{
			var n = s;
			var p = '';
			} else {
			var n = s.substring(0,o);
			var p = s.substring(o);
			}

		var suggest = '<table class="suggest_link" style="width: 100%; border: 0px; padding: 1px;">';
		suggest += '<tr><td onclick="javascript:SuggestClick(\''+n+'\');" align="left" width="40%">' + n + '</td>';
		if (p.length > 0)
		    {
			suggest += '<td onclick="javascript:SuggestClick(\''+n+p+'\')" align="right" style="font-size: xx-small;">' + p + '</td>';
		    }
		suggest += '</tr></table>';
		rval += suggest;
		}
	if (rval != '')
	    {
		rval = '<table class="suggest_link" style="width: 100%; border: 0px; padding: 1px;"><tr><td onclick="javascript:DeleteSuggests();" align="right"><b>X</b></td></tr></table>'+rval;
		} else
		{
        DeleteSuggests();
		}

	var elem = document.getElementById('suggest');
	elem.innerHTML = rval;
	elem.className = "suggest";
	}

function iIncomingData(http_request)
	{
	if (http_request.readyState == 4) {
	    waiting_for_data--;
		WorkingIndicator();

	    var first = http_request.responseText.substring(0,1);
		switch (first)
		    {
		 	case "~":
	     	    {
				target = "plist";
				var res = http_request.responseText.substring(1);
				break;
				}
		 	case "=":
	     	    {
				target = "suggest";
				queued_suggest = false;
				var res = http_request.responseText.substring(1);
				EncloseToSuggestTable(res);
				return;
				}
			default:
			    {
				target = "res";
				var res = EncloseToResultTable(http_request.responseText);
				}
			}
			
		var elem = document.getElementById(target);
		if (elem != null)
			{
			if (http_request.status == 200)
				{
                elem.innerHTML = res;
				} else
				{
                elem.innerHTML = EncloseToResultTable("<h2>HTTP error "+http_request.status+": "+http_request.statusText+"</h2>"
					+' <a href="http://en.wikipedia.org/wiki/List_of_HTTP_status_codes" target="_blank">List of status codes</a>');
				}
			}
		}
	}

function removec(s, t)
	{
	i = s.indexOf(t);
	r = "";
	if (i == -1) return s;
	r += s.substring(0,i) + removec(s.substring(i + t.length), t);
	return r;
	}

function StartTimer()
	{
	starttime=new Date();
	starttime=starttime.getTime();
	CountdownTick();
	}

function StartOrStopTimer()
	{
	if (stopping != true)
	    {stopping = true;}
		else
	        {
			stopping = false;
            refreshinterval = reloadseconds;
			StartTimer();
			}
	}

function CountdownTick()
	{
	if (stopping == true) return;
	nowtime= new Date();
	nowtime=nowtime.getTime();
	secondssinceloaded=(nowtime-starttime)/1000;
	reloadseconds=Math.round(refreshinterval-secondssinceloaded);
	if (refreshinterval>=secondssinceloaded) {
		var timer=setTimeout("CountdownTick()", 1000);
		var elem = document.getElementById("refreshinfo");
		if (elem != null)
			{elem.innerHTML = reloadseconds+" s";}
		}
	else {
	    RefreshNow();
		}
	}

function SwitchDiv(theDiv)
	{
	elem = document.getElementById(theDiv);
	if (elem.style.display == "")
	    {elem.style.display = "none";} else
		{elem.style.display = "";}
	}

function HandleEnter()
	{
	elem = document.getElementById(selected_obj);
	if (elem != null)
		{elem.click();}
	return false;
	}

function WorkingIndicator()
	{
    var elem = document.getElementById("working");
	if (elem != null)
		{
		if (waiting_for_data > 0) {elem.innerHTML = " &nbsp; &nbsp; &nbsp; &nbsp;<img src=\"ajax-loader.gif\" alt=\"...\">";}
			else {elem.innerHTML = "";}
		}
	}

function RefreshNow()
	{
	iPostData("plist","status","1");
	refreshinterval = defrefreshinterval;
	StartTimer();
	}
	
function CmdMsg(q,c,n)
	{
	StartOrStopTimer();
	text = prompt(q+" "+n,"");
	if (text)
		{
		text = removec(text,'"');
		c = c.replace(/%m/g, text);
		c = c.replace(/%n/g, n);
		var elem = document.getElementById('cmdbox');
        elem.value = c;
		SubmitCustomCmd();
		}
    StartOrStopTimer();
	}

function CustomCmd(n)
	{
	var elem = document.getElementById('cmdbox');
	elem.value = n;
	SubmitCustomCmd();
	}

function SubmitCustomCmd()
	{
	if (window.event != null)
		{var evtobj = window.event;}
		else if (window.e != null)
		{var evtobj = window.e;}
	if ((evtobj == null) || (! evtobj.shiftKey))
		{
		var elem = document.getElementById("cmdbox");
		if (document.cmd_form.colors.checked) {col = "1";} else {col = "0";}

		DeleteSuggests();
		iPostData("cmd", elem.value, col);
		return false;
	    }
	}

function SubmitChangeMapOrGametype(what)
	{
	if (what == 0)
	    {
        CustomCmd("g_gametype");
		} else if ((what == 1) || (what == 2))
		{
		var s = document.misc.gtype.value;
		if (s != "")
		    {
			CustomCmd("g_gametype "+s);
			if (what == 2)
				{setTimeout('CustomCmd("map_restart")', 3000);}
			}
		} else if (what == 3)
		{
		var s = document.misc.map.value;
		if ((s != "") && (s != "restart"))
		    {CustomCmd("map "+s);}
		       else {CustomCmd("map_restart");}
		}
	}

function SubmitChangeWeapon(weap, what)
	{
	if (what == -1)
	    {
        CustomCmd(weap);
		} else if ((what == 0) || (what == 1))
		{
		CustomCmd(weap+" "+what);
		}
	}

function SubmitChangePass()
	{
    var s = document.misc.pass.value;
    iPostData("pass", s, 1)
	}

function MapImgShow(what)
	{
    var elem = document.getElementById("mapimg");
	if (elem != null)
		{
		if (what != "") {elem.innerHTML = "<img src=\"maps/"+what+".jpg\" style=\"border: 2px ridge;\">";}
			else {elem.innerHTML = "";}
		}
	}

function SwitchMapVisibility()
	{
    var elem = document.getElementById("mapimg");
	if ((elem != null) && (elem.innerHTML == ""))
	    {
		elem = document.getElementById("mapimgbtn");
		if (elem != null)
			{elem.click();}
		} else
		{
        MapImgShow('');
		}
	return false;
	}

function DeleteResults()
	{
    var elem = document.getElementById("res");
	if (elem != null)
		{
		elem.innerHTML = "";
		}
	}

function DeleteSuggests()
	{
    var elem = document.getElementById("suggest");
	if (elem != null)
		{
		elem.className = "suggest_inv";
		elem.innerHTML = "";
		}
	}

function SuggestInit()
	{
	if (queued_suggest == true)
	    {return;}
	    
	if (window.event != null)
		{var evtobj = window.event;}
		else if (window.e != null)
		{var evtobj = window.e;}

//     document.getElementById('cmdbox').value += ' ' + evtobj.keyCode;

	if ((evtobj != null) &&		// only a-z, 0-9, _, backspace, delete
	    (! (((evtobj.keyCode >= 65) && (evtobj.keyCode <= 90)) ||
	    ((evtobj.keyCode >= 48) && (evtobj.keyCode <= 57)) ||
		 (evtobj.keyCode ==  8) || (evtobj.keyCode == 45) || (evtobj.keyCode == 46) ) ) )
	    return;

    var str = document.getElementById('cmdbox').value;

    if ((str.length > 0) && (str.indexOf(' ') < 0))
	    {
	    iPostData("suggest", escape(str));
	    queued_suggest = true;
		} else {
        DeleteSuggests();
		}

	}

function SuggestClick(val)
	{
	document.getElementById('cmdbox').value = val;
	DeleteSuggests();
	}


// Hotkeys
$.hotkeys.add('g',{type: 'keydown', propagate: false, disableInInput: true}, function(){SubmitChangeMapOrGametype(0);});
$.hotkeys.add('m',{type: 'keydown', propagate: false, disableInInput: true}, function(){SwitchMapVisibility();});
$.hotkeys.add('r',{type: 'keydown', propagate: false, disableInInput: true}, function(){RefreshNow();});
$.hotkeys.add('s',{type: 'keydown', propagate: false, disableInInput: true}, function(){StartOrStopTimer();});
$.hotkeys.add('x',{type: 'keydown', propagate: false, disableInInput: true}, function(){DeleteResults();});


//--></script>
<?php

echo '</head><body class="padded" onload="Startup();">
<span ID="working" style="position: absolute; top: 20px; right: 20px; left: auto; margin:0; padding:0;"></span>
<h1>'.$page_title;

if (count($servers)>1)
	{
	echo ' / <select name="server" class="dropdown" onchange="window.location=\'index.php?server=\'+this.value;">';
	foreach ($servers as $i=>$n)
		{
		$n = explode(' ',$n,3);
		echo '<option value="'.$i.'"'.(($i == $server_id)?' selected':'').'>'.$n[2].'</option>';
		}

	echo '</select>';
	}

echo '</h1>';
$s = explode('/',$lang['page_refresh_remain'],2);
echo '<a href="#" onclick="RefreshNow(); return false" title="Hotkey: R">'.$s[0].'</a> '.$s[1].' <span ID="refreshinfo"></span> &nbsp; &nbsp; | &nbsp; &nbsp;<a href="#" onclick="StartOrStopTimer(); return false" title="Hotkey: S">' . $lang['page_refresh_start_stop'] . '</a><br><br>';

$tmp = false;
if (is_array($custom_links)) {
	echo '<span ID="custom_links">';
	foreach ($custom_links as $clnk)
		{
		if ($clnk!='')
		    {
			$clnk = explode('/',$clnk,2);
			if ($tmp)
            	echo ' &nbsp;&nbsp; | &nbsp;&nbsp; '."\n";
			if ($clnk[1] != '')
				{echo '<a href="'.$clnk[1].'" target="_blank">'.$clnk[0].'</a>';}
				else {echo $clnk[0];}
            $tmp = true;
			} else
 			{
 			echo '<br>';
 			$tmp = false;
 			}
		}
	echo '<br><br>';
	echo '</span>';
	}


if (is_array($custom_cmd)) {
    echo '<span ID="custom_commands">';
	foreach ($custom_cmd as $ccmd)
		{
		if ($ccmd!='')
		    {
			$i = 0;
			$ccmd = explode('/',$ccmd);
		    $c = count($ccmd);
			if (($c % 2) != 0)
			    {
			    echo $ccmd[0].(($c>1)?': &nbsp;':'');
			    $i++;
				}
			for(;$i<$c;$i+=2)
			    {
	            echo '<a href="#" onclick="CustomCmd(\''.$ccmd[$i+1].'\'); return false" title="'.$ccmd[$i+1].'">'.$ccmd[$i].'</a>';
				if ($i+2 < $c)
				    echo '&nbsp; | &nbsp;';
				}
			echo '<br>'."\n";
			} else
 			{
 			echo '<br>';
 			}
		}
	echo '<br>';
	echo '</span>';
	}

echo '<div ID="plist"></div>';
echo '<div ID="res"></div>';


echo '<span ID="input_controls">';

if ($suggest_enable)
	{$suggest_enable = (is_file('commands/'.$game.'.txt'));}

echo '<table width="98%"><tr><td width=100 nowrap>'.$lang['command'].':</td><td width="*"><form method="POST" name="cmd_form" action="#">
<input class="query" ID="cmdbox" type="text" name="cmd" size="80" value="'.htmlspecialchars($lastcmd).'" style="width: 73%" autocomplete="off"'.(($suggest_enable)?' onkeyup="SuggestInit();"':'').'>
<input class="button" type="submit" onclick="SubmitCustomCmd(); return false;" value="'.$lang['confirm'].'">
<input type="checkbox" id="colors" name="colors" value="1"'.(((! isset($_GET['colors']))||((int) $_GET['colors'] != 0))?' checked':'').'><label for="colors"> '.$lang['colorized_output'].'</label>
</form></td></tr>';

echo '<tr><td nowrap>'.$lang['game_type'].':</td><td>
'.(($suggest_enable)?'<div ID="suggest" class="suggest_inv"></div>':'').'
<form method="POST" name="misc" action="#" onsubmit="return HandleEnter();">
<select name="gtype" class="dropdown" onkeydown="selected_obj = \'gtype_apply_after_map\'; return true;">
<option value="" selected>&nbsp;</option>';

foreach ($list_of_gtypes as $gtype)
	{
	$t = explode(' ',$gtype,2);
	echo '<option value="'.$t[0].'">'.$t[1].'</option>';
	}

echo '</select>&nbsp;
<a href="#" onclick="SubmitChangeMapOrGametype(0); return false" title="Hotkey: G">'.$lang['get'].'</a> &nbsp;|&nbsp;
<a href="#" onclick="SubmitChangeMapOrGametype(1); return false" id="gtype_apply_after_map">'.$lang['apply_after_map'].'</a> &nbsp;|&nbsp;
<a href="#" onclick="SubmitChangeMapOrGametype(2); return false" id="gtype_apply_now">'.$lang['apply_now'].'</a>

</td></tr>

<tr><td nowrap>'.$lang['map'].':</td><td>
<select name="map" class="dropdown" onkeydown="selected_obj = \'map_apply_now\'; return true;">
<option value="restart" selected>[restart]</option>';

foreach ($list_of_maps as $map)
	{
	$t = explode(' ',$map,2);
	echo '<option value="'.$t[0].'"'.'>'.$t[1].'</option>';
	}

echo '</select>&nbsp;
<a href="#" onclick="SubmitChangeMapOrGametype(3); return false" id="map_apply_now">'.$lang['apply_now'].'</a>
</td></tr></table>
</span>

<span ID="settings_frame">
<a href="#" onclick="SwitchDiv(\'settings\'); return false">'.$lang['settings'].'</a><br>';


echo '
<div id="settings" style="display: none">
<table><tr><td valign=top nowrap>'.$lang['public_password'].':</td><td>
<input class="query" type="password" size="30" name="pass" value="" onkeydown="selected_obj = \'pass_confirm\'; return true;" autocomplete="off">&nbsp;
<a href="#" onclick="SubmitChangePass(); return false" id="pass_confirm">'.$lang['confirm'].'</a>
';


if (count($list_of_weapons)>0)
	{
	echo '</td></tr>
	<tr><td valign=top nowrap>'.$lang['weapons'].':</td><td>';

	function AddSetting($name)
		{
		global $lang;
		echo '<a href="#" onclick="SubmitChangeWeapon(\''.$name.'\',-1); return false">'.$lang['get'].'</a>&nbsp; |&nbsp;
		<a href="#" onclick="SubmitChangeWeapon(\''.$name.'\',0); return false">'.$lang['turn_off'].'</a>&nbsp; |&nbsp;
		<a href="#" onclick="SubmitChangeWeapon(\''.$name.'\',1); return false">'.$lang['turn_on'].'</a>&nbsp; &nbsp;'
		 .(($lang[$name]=='')?$name:$lang[$name]).'<br>';
		}

	foreach ($list_of_weapons as $weapon)
		{
		AddSetting($weapon);
		}
	}
	
echo '</td></tr></table></div></span></form><br>';

echo '<br><small>Created by Ashus in 2008</small>
</body>
</html>';
?>
