<?php
// global settings (all games / all servers)

$refresh_rate = 40;				// enter a number of seconds to automatically refresh the window in

$interface_language = 'en';
/* $interface_language
	en - english
	cz - czech
	es - spanish
	fr - french
	ge - german
	hu - hungarian
	it - italian
	nl - dutch
	no - norwegian
	pl - polish
	rs - serbian
	sk - slovak
*/


$suggest_enable = true;			// enable command and variable hints
$suggest_partial = false;		// enable only partial matches of commands; if disabled, compared to the beginning only

$changepass_enable = true;		// enable changing admin passwords
$changepass_minchars = 6;		// minimum password length
$pw_salt = '$1$Rs7Et5Tf';		// unique password encryption parameter; if changed, all passwords have to be reset

$log_enable = false;			// enable logging of commands
$rcon_pw_protect = true;		// disable reading/changing rcon password


$geoip_resolve = 2;
/* $geoip_resolve
	0 to disable
	1 to enable internal PHP function (apache module libapache_mod_geoip needed)
	2 to enable external system command 'geoiplookup IP' (result is expected in format 'uninteresting: CC, uninteresting', where CC is 2-letter Country code; if none received, '--' for unknown is returned)
	3 to enable external system command 'geoip-lookup IP' (returns only CC)
	4 to enable internal PHP library (first download and extract GeoIP.dat to php rcon root dir)
*/

$geoip_flags = true;			// true | false; if you want to enable flag images, enter true
$geoip_local_network = 'AQ';	// if someone comes from internal IP range (10.0.0.0/8, 192.168.0.0/16, 172.16.0.0/12), assign a country code (use CAPITAL letters !on unix!)


// Custom links - favourite links
// string syntax: (button text)/(hyperlink opened in new window)
//            or: (title)           // insert a text to an extra line
//            or: (empty line)      // to leave an extra linebreak, duh :)

//$custom_links[] = 'RCon log/log.tpl.php';
//$custom_links[] = 'Country codes/http://ftp.ics.uci.edu/pub/websoft/wwwstat/country-codes.txt';

// Custom commands - favourite commands
// string syntax: (button text)/(command)
//            or: (title)           // insert a text to an extra line
//            or: (empty line)      // to leave an extra linebreak, duh :)
//            or: (title)/(button text)/(command)/(button text 2)/(command 2)/.....n   // displayed as  Title:  Button1  |  Button2  |  ...n

//$custom_cmd[] = 'Update PunkBuster/pb_sv_update';


// Custom commands - next to all players
// string syntax: (button text)/(command)/(player ID offset)
// player id offset should be 0, use 1 for PunkBuster commands
//
// if command contains %m, a query pops up for you to enter a message
//    then you have to specify position for player ID by %n
//    and remember to enclose the message into quotes: &quot;

// $custom_cmds[] = 'Temp ban/tempbanclient/0';
// $custom_cmds[] = 'Ban/banclient/0';
// $custom_cmds[] = 'PB Kick/pb_sv_kick/1';
// $custom_cmds[] = 'PB Ban/pb_sv_ban/1';
// $custom_cmds[] = 'PB Kick+msg/pb_sv_kick %n 15 &quot;%m&quot;/1'; //kick for 15 minutes and tell the others the reason
// $custom_cmds[] = 'PB Ban+msg/pb_sv_ban %n &quot;%m&quot;/1'; //ban the guid, tell others the reason


?>
