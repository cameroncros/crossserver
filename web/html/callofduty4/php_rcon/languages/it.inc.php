<?php

// PHP RCON language file
// ITALIAN 2.0a
// by PietroTC & Nazgul

$lang['confirm'] = 'OK';
$lang['command'] = 'Comando';
$lang['result'] = 'Risultato';
$lang['game_type'] = 'Modalità di gioco';
$lang['map'] = 'Mappa';
$lang['settings'] = 'Impostazioni';
$lang['get'] = 'Ottieni';
$lang['turn_off'] = 'Off';
$lang['turn_on'] = 'On';
$lang['public_password'] = 'Password pubblica';
$lang['weapons'] = 'Armi';

$lang['connection_error'] = 'Errore durante il collegamento al server di gioco.';
$lang['log_write_error'] = 'Errore durante la creazione del file di log. Controlla i tuoi permessi di accesso (scrittura e lettura).';
$lang['geoipdat_error'] = 'Errore durante l\' apertura del file GeoIP.dat.';
$lang['rcon_pw_protected_error'] = 'Un tentativo di visualizzare o modificare la password di RCON è stato sventato e loggato.';

$lang['msg_prefix_all'] = 'all';
$lang['msg_prefix_priv'] = 'priv.';
$lang['kick'] = 'Kick';
$lang['say'] = 'Parla';
$lang['whisper'] = 'Sussurra';
$lang['colorized_output'] = 'output colorato';
$lang['page_refresh_remain'] = 'Aggiorna fra ';
$lang['page_refresh_start_stop'] = 'Pausa/Riprendi';
$lang['apply_after_map'] = 'Applica dopo la mappa';
$lang['apply_now'] = 'Applica adesso';
$lang['enter_message'] = 'Inserisci il messaggio per';
$lang['enter_public_message'] = 'Inserisci un messaggio pubblico';

$lang['scr_allow_fraggrenades'] = 'Granate';
$lang['scr_allow_smokegrenades'] = 'Granate fumogene';
$lang['scr_allow_shotgun'] = 'Shotgun';

$lang['login_logged_as'] = 'Login eseguito come';
$lang['login_change_password'] = 'modifica password';
$lang['login_logout'] = 'Uscita';
$lang['login_name'] = 'Utente';
$lang['login_password'] = 'Password';
$lang['login_please_enter'] = 'Inserisci il tuo nome utente e la tua password';

$lang['changepass_title'] = 'Modifica password';
$lang['changepass_old_password'] = 'Vecchia password';
$lang['changepass_new_password'] = 'Nuova password';
$lang['changepass_confirm_new_password'] = 'Conferma nuova password';
$lang['changepass_success'] = 'Password modificata con successo.';
$lang['changepass_error_oldpw'] = 'Errore nei dati immessi : la vecchia password non corrisponde.';
$lang['changepass_error_newpw'] = 'Errore nei dati immessi : le nuove passwords non corrispondono.';
$lang['changepass_error_newpw_short'] = 'Errore nei dati immessi : la nuova password deve essere lunga almeno / caratteri.'; // (/ will be replaced by a number)
$lang['changepass_write_error'] = 'Errore nel salvare le modifiche al file users.inc.php. Controlla i tuoi permessi di accesso (scrittura e lettura).';

?>
