<?php

// PHP RCON language file
// SPANISH 1.9
// by TaRTeSSio

$lang['confirm'] = 'Aceptar';
$lang['command'] = 'Comando';
$lang['result'] = 'Resultado';
$lang['game_type'] = 'Tipo de juego';
$lang['map'] = 'Mapa';
$lang['settings'] = 'Configuraciones';
$lang['get'] = 'Poner';
$lang['turn_off'] = 'No';
$lang['turn_on'] = 'Si';
$lang['public_password'] = 'Clave publica';
$lang['weapons'] = 'Armas';

$lang['connection_error'] = 'Error mienstras conectaba con el servidor de juegos; el servidor esta caido o esta cambiando de mapa en este momento.';
$lang['log_write_error'] = 'Error creando un archivo log. Por favor comprueba los derechos de acceso.';
$lang['geoipdat_error'] = 'Error abriendo GeoIP.dat.';
$lang['rcon_pw_protected_error'] = 'Un atentado al obtener o dar una contraseña de rcon, ha sido prevenido y logado.';

$lang['msg_prefix_all'] = 'todo';
$lang['msg_prefix_priv'] = 'priv.';
$lang['kick'] = 'Expulsar';
$lang['say'] = 'Decir';
$lang['whisper'] = 'Susurrar';
$lang['colorized_output'] = 'Sin colorear';
$lang['page_refresh_remain'] = 'Refrescar/dentro';
$lang['page_refresh_start_stop'] = 'Parar/pausar';
$lang['apply_after_map'] = 'Aplicar despues del mapa';
$lang['apply_now'] = 'Aplicar ahora';
$lang['enter_message'] = 'Por favor introduce un mesaje para';
$lang['enter_public_message'] = 'Por favor introduce un mensaje publico';

$lang['scr_allow_fraggrenades'] = 'Granadas';
$lang['scr_allow_smokegrenades'] = 'Granadas de humo';
$lang['scr_allow_shotgun'] = 'Escopeta';

$lang['login_logged_as'] = 'Logado como';
$lang['login_logout'] = 'Deslogar';
$lang['login_name'] = 'Nombre';
$lang['login_password'] = 'Clave';
$lang['login_please_enter'] = 'Por favor introduce tu ID de logado';

?>