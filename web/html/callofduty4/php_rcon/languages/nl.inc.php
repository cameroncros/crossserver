<?php

// PHP RCON language file
// DUTCH 1.9
// by HippoTraxius

$lang['confirm'] = 'Bevestig';
$lang['command'] = 'Commando';
$lang['result'] = 'Resultaat';
$lang['game_type'] = 'Spel type';
$lang['map'] = 'Map';
$lang['settings'] = 'Instellingen';
$lang['get'] = 'Ophalen';
$lang['turn_off'] = 'Uit';
$lang['turn_on'] = 'Aan';
$lang['public_password'] = 'Publiek paswoord';
$lang['weapons'] = 'Wapens';

$lang['connection_error'] = 'Fout tijdens verbinden met de gameserver: de server staat uit of wisselt een map';
$lang['log_write_error'] = 'Fout bij het maken van de logfile. Controleer uw bestandsrechten.';
$lang['geoipdat_error'] = 'Fout bij het openen van het bestand GeoIP.dat.';
$lang['rcon_pw_protected_error'] = 'Een poging om het rcon wachtwoord op te vragen of te wijzigen is voorkomen en geregistreerd.';

$lang['msg_prefix_all'] = 'Iedereen';
$lang['msg_prefix_priv'] = 'Prive';
$lang['kick'] = 'Kick';
$lang['say'] = 'Zeg';
$lang['whisper'] = 'Fluister';
$lang['colorized_output'] = 'Kleuren weergeven';
$lang['page_refresh_remain'] = 'Ververs/in';
$lang['page_refresh_start_stop'] = 'Stop/Verder';
$lang['apply_after_map'] = 'Bij start van volgende map toepassen ';
$lang['apply_now'] = 'Direct toepassen';
$lang['enter_message'] = 'Type uw bericht voor';
$lang['enter_public_message'] = 'Type uw publieke bericht';

$lang['scr_allow_fraggrenades'] = 'Granaat';
$lang['scr_allow_smokegrenades'] = 'Rookgranaat';
$lang['scr_allow_shotgun'] = 'Geweer';

$lang['login_logged_as'] = 'Ingelogd als';
$lang['login_logout'] = 'Uitloggen';
$lang['login_name'] = 'Naam';
$lang['login_password'] = 'Paswoord';
$lang['login_please_enter'] = 'Vul uw inlog gegevens in';

?>
