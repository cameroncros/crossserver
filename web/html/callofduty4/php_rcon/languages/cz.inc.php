<?php

// PHP RCON language file
// CZECH 2.0
// by Ashus

$lang['confirm'] = 'OK';
$lang['command'] = 'Příkaz';
$lang['result'] = 'Výsledek';
$lang['game_type'] = 'Typ hry';
$lang['map'] = 'Mapa';
$lang['settings'] = 'Nastavení';
$lang['get'] = 'Zjistit';
$lang['turn_off'] = 'Vypnout';
$lang['turn_on'] = 'Zapnout';
$lang['public_password'] = 'Veřejné heslo';
$lang['weapons'] = 'Zbraně';

$lang['connection_error'] = 'Nelze se připojit k serveru; buď neběží, nebo se možná právě mění mapa.';
$lang['log_write_error'] = 'Nelze zapisovat do logu. Prosím zkontrolujte přístupová práva.';
$lang['geoipdat_error'] = 'Nastala chyba při otevírání GeoIP.dat.';
$lang['rcon_pw_protected_error'] = 'Bylo zabráněno pokusu o zjištění nebo změnu rcon hesla - a byl zaznamenán.';

$lang['msg_prefix_all'] = 'vsem';
$lang['msg_prefix_priv'] = 'soukr.';
$lang['kick'] = 'Kopnout';
$lang['say'] = 'Říct';
$lang['whisper'] = 'Šeptat';
$lang['colorized_output'] = 'výsledek v barvách';
$lang['page_refresh_remain'] = 'Obnovení/za';
$lang['page_refresh_start_stop'] = 'Zastavit/pokračovat';
$lang['apply_after_map'] = 'Použít po mapě';
$lang['apply_now'] = 'Použít hned';
$lang['enter_message'] = 'Prosím zadejte text zprávy pro';
$lang['enter_public_message'] = 'Prosím zadejte text veřejné zprávy';

$lang['scr_allow_fraggrenades'] = 'Granáty';
$lang['scr_allow_smokegrenades'] = 'Kouřové granáty';
$lang['scr_allow_shotgun'] = 'Brokovnice';

$lang['login_logged_as'] = 'Přihlášen jako';
$lang['login_change_password'] = 'změnit heslo';
$lang['login_logout'] = 'odhlásit se';
$lang['login_name'] = 'Jméno';
$lang['login_password'] = 'Heslo';
$lang['login_please_enter'] = 'Prosím, zadejte své přihlašovací údaje';

$lang['changepass_title'] = 'Změna hesla';
$lang['changepass_old_password'] = 'Staré heslo';
$lang['changepass_new_password'] = 'Nové heslo';
$lang['changepass_confirm_new_password'] = 'Potvrzení nového hesla';
$lang['changepass_success'] = 'Heslo úspěšně změněno.';
$lang['changepass_error_oldpw'] = 'Chyba v zadání: staré heslo nesouhlasí.';
$lang['changepass_error_newpw'] = 'Chyba v zadání: nová hesla nesouhlasí.';
$lang['changepass_error_newpw_short'] = 'Chyba v zadání: nové heslo musí mít aspoň / znaků.';
$lang['changepass_write_error'] = 'Nelze uložit změny do users.inc.php. Prosím zkontrolujte přístupová práva.';

?>
