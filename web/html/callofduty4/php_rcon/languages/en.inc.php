<?php

// PHP RCON language file
// ENGLISH 2.0
// by Ashus

$lang['confirm'] = 'OK';
$lang['command'] = 'Command';
$lang['result'] = 'Result';
$lang['game_type'] = 'Game type';
$lang['map'] = 'Map';
$lang['settings'] = 'Settings';
$lang['get'] = 'Get';
$lang['turn_off'] = 'Off';
$lang['turn_on'] = 'On';
$lang['public_password'] = 'Public password';
$lang['weapons'] = 'Weapons';

$lang['connection_error'] = 'Error while connecting to gameserver; server is down or the map may be changing right now.';
$lang['log_write_error'] = 'Error creating logfile. Please check your access rights.';
$lang['geoipdat_error'] = 'Error while opening GeoIP.dat.';
$lang['rcon_pw_protected_error'] = 'An attempt to get or set rcon password was prevented and logged.';

$lang['msg_prefix_all'] = 'all';
$lang['msg_prefix_priv'] = 'priv.';
$lang['kick'] = 'Kick';
$lang['say'] = 'Say';
$lang['whisper'] = 'Whisper';
$lang['colorized_output'] = 'colorized output';
$lang['page_refresh_remain'] = 'Refresh/in'; // text after / is not a refresh link, only text before number
$lang['page_refresh_start_stop'] = 'Stop/resume';
$lang['apply_after_map'] = 'Apply after map';
$lang['apply_now'] = 'Apply now';
$lang['enter_message'] = 'Please enter a message for';
$lang['enter_public_message'] = 'Please enter a public message';

$lang['scr_allow_fraggrenades'] = 'Grenades';
$lang['scr_allow_smokegrenades'] = 'Smoke grenades';
$lang['scr_allow_shotgun'] = 'Shotgun';

$lang['login_logged_as'] = 'Logged as';
$lang['login_change_password'] = 'change password';
$lang['login_logout'] = 'logout';
$lang['login_name'] = 'Name';
$lang['login_password'] = 'Password';
$lang['login_please_enter'] = 'Please enter your login ID';

$lang['changepass_title'] = 'Change password';
$lang['changepass_old_password'] = 'Old password';
$lang['changepass_new_password'] = 'New password';
$lang['changepass_confirm_new_password'] = 'Confirm new password';
$lang['changepass_success'] = 'Password successfully changed.';
$lang['changepass_error_oldpw'] = 'Entry error: old password doesn\'t match.';
$lang['changepass_error_newpw'] = 'Entry error: new passwords do not match.';
$lang['changepass_error_newpw_short'] = 'Entry error: new password must be at least / characters long.';
$lang['changepass_write_error'] = 'Error saving changes to users.inc.php. Please check your access rights.';

?>
