<?php

// PHP RCON language file
// SERBIAN 2.0
// by Fanatic

$lang['confirm'] = 'OK';
$lang['command'] = 'Konzola';
$lang['result'] = 'Rezultat';
$lang['game_type'] = 'Trenutna vrsta igre';
$lang['map'] = 'Mapa';
$lang['settings'] = 'Zakljucajte server';
$lang['get'] = 'Izvrsi';
$lang['turn_off'] = 'Iskljuci';
$lang['turn_on'] = 'Ukljuci';
$lang['public_password'] = '';
$lang['weapons'] = 'Oruzije';

$lang['connection_error'] = 'Greska pri konekciji na gameserver; server neradi ili se menja mapa.';
$lang['log_write_error'] = 'Greska pri kreiranju log fajla. Proverite vasa prava.';
$lang['geoipdat_error'] = 'Greska pri ucitavanju GeoIP.dat.';
$lang['rcon_pw_protected_error'] = 'Pokusaj da pogledate ili promenite rcon password je odbacena.';

$lang['msg_prefix_all'] = 'sve';
$lang['msg_prefix_priv'] = 'privatni.';
$lang['kick'] = 'Kikuj';
$lang['say'] = 'Posalji pm svima';
$lang['whisper'] = 'Posalji pm';
$lang['colorized_output'] = 'Kolorizuj poruke';
$lang['page_refresh_remain'] = 'Refresujem/za';
$lang['page_refresh_start_stop'] = 'Stopiraj/nastavi';
$lang['apply_after_map'] = 'Mapa posle ove';
$lang['apply_now'] = 'Promeni mapu odmah';
$lang['enter_message'] = 'Molimo vas unesite poruku za';
$lang['enter_public_message'] = 'Molimo vas unesite public poruku';

$lang['scr_allow_fraggrenades'] = 'Grantate';
$lang['scr_allow_smokegrenades'] = 'Dimne bombe';
$lang['scr_allow_shotgun'] = 'Sacmare';

$lang['login_logged_as'] = 'Ulogovani ste kao';
$lang['login_change_password'] = 'Promeni lozinku';
$lang['login_logout'] = 'Izloguj se';
$lang['login_name'] = 'Ime';
$lang['login_password'] = 'Password';
$lang['login_please_enter'] = 'Molimo vas da unesete vase podatke';

$lang['changepass_title'] = 'Promeni lozinku';
$lang['changepass_old_password'] = 'Stara lozinka';
$lang['changepass_new_password'] = 'Nova lozinka';
$lang['changepass_confirm_new_password'] = 'Potvrdi promenu lozinke';
$lang['changepass_success'] = 'Lozinka uspesno promenjena.';
$lang['changepass_error_oldpw'] = 'Greska: Stara lozinke se nepoklapaju.';
$lang['changepass_error_newpw'] = 'Greska: Nova lozinke se nepokapaju';
$lang['changepass_error_newpw_short'] = 'Greska: Nova lozinka mora biti dugacka najmanje / karaktera.';
$lang['changepass_write_error'] = 'Greska pri upisu u fajl users.inc.php. Molimo vas proverite svoja prava.';

?>
