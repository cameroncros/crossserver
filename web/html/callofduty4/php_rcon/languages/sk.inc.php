<?php

// PHP RCON language file
// SLOVAK 2.0
// by Eddie

$lang['confirm'] = 'OK';
$lang['command'] = 'Príkaz';
$lang['result'] = 'Výsledok';
$lang['game_type'] = 'Typ hry';
$lang['map'] = 'Mapa';
$lang['settings'] = 'Nastavenie';
$lang['get'] = 'Zistiť';
$lang['turn_off'] = 'Vypnúť';
$lang['turn_on'] = 'Zapnúť';
$lang['public_password'] = 'Verejné heslo';
$lang['weapons'] = 'Zbrane';

$lang['connection_error'] = 'Nedá sa pripojiť k serveru; buď nebeží, alebo sa možno práve mení mapa.';
$lang['log_write_error'] = 'Nie je možné zapisovať do logu. Prosím, skontrolujte prístupové práva.';
$lang['geoipdat_error'] = 'Nastala chyba pri otváraní GeoIP.dat.';
$lang['rcon_pw_protected_error'] = 'Bolo zabránené pokusu o zistenie alebo zmenu rcon hesla - a bol zaznamenaný.';

$lang['msg_prefix_all'] = 'vsetkym';
$lang['msg_prefix_priv'] = 'sukromne';
$lang['kick'] = 'Kopnúť';
$lang['say'] = 'Povedať';
$lang['whisper'] = 'Šepkať';
$lang['colorized_output'] = 'výsledok vo farbách';
$lang['page_refresh_remain'] = 'Obnovenie/za';
$lang['page_refresh_start_stop'] = 'Zastaviť/pokračovať';
$lang['apply_after_map'] = 'Použiť po mape';
$lang['apply_now'] = 'Použiť hneď';
$lang['enter_message'] = 'Prosím, zadajte text správy pre';
$lang['enter_public_message'] = 'Prosím, zadajte text verejnej správy';

$lang['scr_allow_fraggrenades'] = 'Granáty';
$lang['scr_allow_smokegrenades'] = 'Dymové granáty';
$lang['scr_allow_shotgun'] = 'Brokovnica';

$lang['login_logged_as'] = 'Prihlásený ako';
$lang['login_change_password'] = 'zmeniť heslo';
$lang['login_logout'] = 'odhlásiť sa';
$lang['login_name'] = 'Meno';
$lang['login_password'] = 'Heslo';
$lang['login_please_enter'] = 'Prosím, zadajte svoje prihlasovacie údaje';

$lang['changepass_title'] = 'Zmena hesla';
$lang['changepass_old_password'] = 'Staré heslo';
$lang['changepass_new_password'] = 'Nové heslo';
$lang['changepass_confirm_new_password'] = 'Potvrdenie nového hesla';
$lang['changepass_success'] = 'Heslo úspešne zmenené.';
$lang['changepass_error_oldpw'] = 'Chyba v zadaní: staré heslo nesedí.';
$lang['changepass_error_newpw'] = 'Chyba v zadaní: nové heslá nesedia.';
$lang['changepass_error_newpw_short'] = 'Chyba v zadaní: nové heslo musí mať aspoň / znakov.';
$lang['changepass_write_error'] = 'Nedá sa uložiť zmeny do users.inc.php. Prosím skontrolujte prístupové práva.';

?>
